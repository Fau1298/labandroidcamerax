package com.fau.labandroidcamerax

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var myButtonSend : Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bt_enviar.setOnClickListener{
            sendEmail()
        }

        bt_main_foto.setOnClickListener{
            activateCamera()
        }
    }
    
    private fun activateCamera(){
        val myIntent = Intent(this, CamaraActivity::class.java)
        startActivity(myIntent)
    }

    private fun sendEmail(){
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"))

        var msg: String? = pt_nombre.text.toString() + " tiene los siguientes síntomas: \n"

        if(sw_fiebre.isChecked){
            msg += "-> Fiebre \n"
        }

        if(sw_cansancio.isChecked){
            msg += "-> Cansancio \n"
        }

        if(sw_gusto.isChecked){
            msg += "-> Pérdida del gusto \n"
        }

        if(sw_migranna.isChecked){
            msg += "-> Migraña \n"
        }

        if(sw_olfato.isChecked){
            msg += "-> Pérdida del olfato \n"
        }

        if(sw_tos.isChecked){
            msg += "->Tos"
        }

        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Test de COVID-19")
        emailIntent.putExtra(Intent.EXTRA_TEXT, msg)
        startActivity(Intent.createChooser(emailIntent, "Seleccione una aplicación"))
    }
}










