package com.fau.labandroidcamerax

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_verfoto.*
import java.io.File
import java.io.InputStream


private var myFile : File? = null

class VerfotoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verfoto)

        bt_save.setOnClickListener{ savePic() }
        bt_send.setOnClickListener { sendPic() }

        //recibir el dato
        val datos = this.intent.extras
        myFile = datos?.get("myFile") as File

        val newURI : String? = Uri.fromFile(myFile).toString().substring(7)

        Glide.with(this).load(newURI).into(imageView3)
        imageView3.setImageResource(R.drawable.ic_launcher_background)
    }

    private fun savePic(){
        Toast.makeText(baseContext, "Foto guardada", Toast.LENGTH_SHORT).show()
    }

    private fun sendPic(){
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Test de COVID-19")
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(myFile))
        startActivity(Intent.createChooser(emailIntent, "Seleccione una aplicación"))
    }
}